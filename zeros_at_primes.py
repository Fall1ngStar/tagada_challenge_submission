from numpy import zeros
from math import sqrt

def zerosAtPrimes(array):
    """
    entree array: array
    sortie: array
    post-cond: Remplace toutes les valeurs aux indices premiers du tableau array par 0 et renvoie le nouveau tableau
    """
    size = len(array)
    if size > 2:
        primals = zeros(size)
        output = zeros(size)

        output[0] = array[0]
        output[1] = array[1]

        for j in range(4, size, 2):
            output[j] = array[j]

        for i in range(3, int(sqrt(size)) + 1, 2):
            if primals[i] != 1:
                for j in range(i**2, size, 2*i):
                    primals[j] = 1
                    output[j] = array[j]
    else:
        output = array
    return(output)
